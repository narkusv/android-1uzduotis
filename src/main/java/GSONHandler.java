import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class GSONHandler {

    public static ArrayList<MoreAppEntry> getAppEntryListFromJson(String json){
        Gson gson = new Gson();
        return gson.fromJson(json, new TypeToken<ArrayList<MoreAppEntry>>(){}.getType());
    }

}
