import java.util.ArrayList;

public interface IApplicationPrinter {

    public void printApps(ArrayList<MoreAppEntry> apps);
}
