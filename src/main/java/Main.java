
import com.google.gson.Gson;
import org.asynchttpclient.AsyncCompletionHandler;
import org.asynchttpclient.Response;
import org.fusesource.jansi.AnsiConsole;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class Main{

    //Git URL https://narkusv@bitbucket.org/narkusv/android-1uzduotis.git
    public static void main(String[] args) {
        AnsiConsole.systemInstall();
        Printer printer = new Printer();
        

        AsyncCompletionHandler<Response> handler = new AsyncCompletionHandler<Response>() {
            @Override
            public Response onCompleted(Response response) throws Exception {
                printer.printApps(GSONHandler.getAppEntryListFromJson(response.getResponseBody()));

                return response;
            }
        };

        NetworkDownloader down = new NetworkDownloader();
        down.startDownloadTask(handler);



    }





}
