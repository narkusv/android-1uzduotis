public class MoreAppEntry {
    public String appName;
    public String appURL;
    public String appPhotoURL;

    public MoreAppEntry(String appName, String appURL, String appPhotoURL) {
        this.appName = appName;
        this.appURL = appURL;
        this.appPhotoURL = appPhotoURL;
    }
}
