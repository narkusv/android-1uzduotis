import static org.asynchttpclient.Dsl.*;

import org.asynchttpclient.AsyncCompletionHandler;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.ListenableFuture;
import org.asynchttpclient.Response;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.concurrent.CompletableFuture;

public class NetworkDownloader {
    public static final String GMAURL = "http://107.155.116.45/FCMServer/gma_controller.php?action=returnListOfApps";

    public void startDownloadTask(AsyncCompletionHandler<Response> asyncCompletionHandler){
        asyncHttpClient()
                .prepareGet(GMAURL)
                .execute(asyncCompletionHandler);
    }

}
