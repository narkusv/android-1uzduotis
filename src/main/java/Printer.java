import java.util.ArrayList;

import static org.fusesource.jansi.Ansi.ansi;

public class Printer implements IApplicationPrinter{



    @Override
    public void printApps(ArrayList<MoreAppEntry> apps) {
        for (MoreAppEntry app : apps) {
            
            System.out.println(ansi().fgBlue().a(app.appName).reset());
        }

    }
}
